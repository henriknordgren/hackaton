﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton.ViewModels
{
    public class AddPersonViewModel : Screen
    {
        public INavigationService NavigationService { get; set; }

        public AddPersonViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        public void BackButton()
        {
            NavigationService.GoBack();
        }
    }
}
