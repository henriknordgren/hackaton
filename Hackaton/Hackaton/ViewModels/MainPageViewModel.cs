﻿using Caliburn.Micro;
using Hackaton.Common;
using Hackaton.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hackaton.ViewModels
{
    public class MainPageViewModel : Screen
    {
        private IObservableCollection<PersonViewModel> _persons;
        private IObservableCollection<string> _strings;
        private INavigationService _navigationService;

        public CancellationTokenSource cts;


        public MainPageViewModel(INavigationService navigationService)
        {
            cts = new CancellationTokenSource();
            _navigationService = navigationService;
        }

        public IObservableCollection<PersonViewModel> Persons
        {
            get
            {
                return _persons;
            }
            set
            {
                _persons = value;
                NotifyOfPropertyChange(() => Persons);
            }
        }

        public IObservableCollection<string> Strings
        {
            get
            {
                return _strings;

            }
            set
            {
                _strings = value;
                NotifyOfPropertyChange(() => Strings);
            }
        }
        protected async override void OnActivate()
        {            
            base.OnActivate();
            _persons = new BindableCollection<PersonViewModel>();
            _strings = new BindableCollection<string>();
            GeneratePersons(10);         
            NotifyOfPropertyChange(() => Persons);

            await PeriodicTask.Run(() =>
                {
                   foreach(var p in Persons)
                   {
                       p.LastUpdated = DateTime.Now;
                   }
                }, TimeSpan.FromSeconds(5), cts.Token);

        }

        private void GeneratePersons(int noOfPersons)
        {            
            for(var i=0; i< noOfPersons; i++)
            {
                Persons.Add(new PersonViewModel { FirstName = "First" + i, LastName = "Last" + i, LastUpdated = DateTime.Now });          

            }
        }

        public void ApproveCommand()
        {
            _navigationService.NavigateToViewModel<AddPersonViewModel>();
        }
    }
}
