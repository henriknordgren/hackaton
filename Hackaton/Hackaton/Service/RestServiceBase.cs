﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography.Certificates;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

namespace HijTime.WinPhone.Services
{
    public abstract class RestServiceBase
    {
        private string _baseUri;

        public RestServiceBase()
        {
#if DEBUG
            _baseUri = @"http://192.168.90.127";
#else 
                _baseUri = @"https://ht.hiqkna.se/api/";
#endif
        }

        public async Task<T> GetAsync<T>(object[] parameters, string serviceUrl)
        {
            var requestUri = String.Format(serviceUrl, parameters);
            return await GetAsync<T>(requestUri);
        }

        public async Task<T> GetAsync<T>(string serviceUrl)
        {
            string requestUri;

            using (var handler = GetHttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler))
                {
                    requestUri = String.Concat(_baseUri, serviceUrl);
                    var res = await httpClient.GetStringAsync(new Uri(requestUri));
                    return JsonConvert.DeserializeObject<T>(res);
                }
            }
        }

        public async Task<T> PostAsync<T>(string serviceUrl, List<KeyValuePair<string, string>> parameters)
        {
            var requestUri = String.Concat(_baseUri, serviceUrl);
            using (var handler = GetHttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler))
                {
                    var content = new HttpFormUrlEncodedContent(parameters);

                    var res = await httpClient.PostAsync(new Uri(requestUri), content);
                    var resultContent = await res.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(resultContent);
                }
            }
        }

        public async Task PostAsync(string serviceUrl, List<KeyValuePair<string, string>> parameters)
        {
            var requestUri = String.Concat(_baseUri, serviceUrl);
            using (var handler = GetHttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler))
                {
                    var content = new HttpFormUrlEncodedContent(parameters);

                    await httpClient.PostAsync(new Uri(requestUri), content);
                }
            }
        }

        public async Task DeleteAsync(string serviceUrl, object[] parameters)
        {
            using (var handler = GetHttpClientHandler())
            {
                using (var httpClient = new HttpClient(handler))
                {
                    var requestUri = String.Concat(_baseUri, String.Format(serviceUrl, parameters));
                    await httpClient.DeleteAsync(new Uri(requestUri));
                }
            }
        }

        private HttpBaseProtocolFilter GetHttpClientHandler()
        {
            var filter = new HttpBaseProtocolFilter();
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.IncompleteChain);
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.Expired);
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.Untrusted);
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.InvalidName);
            return filter;
        }
    }
}
