﻿using Hackaton.Models;
using HijTime.WinPhone.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton.Service
{
    public class HackatonService : RestServiceBase
    {
        public async Task<Person> GetApprovement(string resourceId, int year, int month)
        {
            const string serviceUri = @"approvement/resource/{0}/date/{1}-{2}-01";

            return await base.GetAsync<Person>(new object[3] { resourceId, year, month }, serviceUri);
        }
    }
}
